# Test voting system project!

👉 This project demonstrates how my mind was blowed out from no idea about how does blockchain works and to communicating with etherium network and get 100% coverage of fresh contract created.🦾

## Clone repo and run:
```shell
yarn
```

or

```shell
npm install
```

Create .env file in root with your secrets it this format:

```
// .env
ALCHEMY_RINKEBY_URL=...
ACCOUNT_PRIVATE_KEY=...
RINKEBY_DEPLOYED_CONTRACT_ADDRESS=...
```

You can add RINKEBY_DEPLOYED_CONTRACT_ADDRESS after contract will be deployed!

## Try running some of the default hardhat tasks:

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
npx hardhat help
```

## Or custom:

```shell
node scripts/deploy.js
npx hardhat run scripts/deploy.js --network rinkeby
npx hardhat createVoting --network rinkeby
npx hardhat getVotingInfo --network rinkeby --votingid 0 //your voting id
npx hardhat joinVoting --network rinkeby --votingid 0 //specify votingid
npx hardhat vote --network rinkeby --votingid 0 --candidate 0xd5931b1564043e79b9c777777e9f032235fc7777 // your votingid and candidate required
npx hardhat finishVoting --network rinkeby --votingid 0 // voting id required
npx hardhat withdrawComission --network rinkeby --toaddress 0xd5931b1564043e79b9c777777e9f032235fc7777 // take profit, target address required
```

## Run tests
```shell
npx hardhat coverage
```