const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("VotingSystem contract", () => {
  let VotingSystem,
    owner,
    addr1,
    addr2,
    addr3,
    addr4,
    addr5,
    addr6,
    votingSystem;
  const THREE_DAYS = 60 * 60 * 24 * 3;

  beforeEach(async () => {
    VotingSystem = await ethers.getContractFactory("VotingSystem");
    [owner, addr1, addr2, addr3, addr4, addr5, addr6] =
      await ethers.getSigners();
    votingSystem = await VotingSystem.deploy();
    await votingSystem.deployed();
    // create 1st voting with 1,2,3 as candidates,
    // could be finished,
    // users addr1 and addr2 did not vote
    await votingSystem.createVoting([
      addr1.address,
      addr2.address,
      addr3.address,
    ]);
    await votingSystem.connect(addr3).vote(0, addr1.address, {
      value: ethers.utils.parseEther("0.01"),
    });
    await votingSystem.connect(addr4).vote(0, addr2.address, {
      value: ethers.utils.parseEther("0.01"),
    });
    await votingSystem.connect(addr5).vote(0, addr3.address, {
      value: ethers.utils.parseEther("0.01"),
    });
    await votingSystem.connect(addr6).vote(0, addr1.address, {
      value: ethers.utils.parseEther("0.01"),
    });
    // create 2nd voting with 1,2,3 as candidates,
    // finished
    // 2 winners by votes
    await votingSystem.createVoting([
      addr1.address,
      addr2.address,
      addr3.address,
    ]);
    await votingSystem.connect(addr1).vote(1, addr2.address, {
      value: ethers.utils.parseEther("0.01"),
    });
    await votingSystem.connect(addr2).vote(1, addr1.address, {
      value: ethers.utils.parseEther("0.01"),
    });
    await votingSystem.connect(addr3).vote(1, addr1.address, {
      value: ethers.utils.parseEther("0.01"),
    });
    await votingSystem.connect(addr4).vote(1, addr2.address, {
      value: ethers.utils.parseEther("0.01"),
    });
    await ethers.provider.send("evm_increaseTime", [THREE_DAYS]);
    await votingSystem.finishVoting(1);
  });

  describe("Create voting", () => {
    it("should create voting by owner", async () => {
      await votingSystem.createVoting([
        addr1.address,
        addr2.address,
        addr3.address,
      ]);
    });
    it("should create voting by owner with duplicated addresses", async () => {
      await votingSystem.createVoting([
        addr1.address,
        addr2.address,
        addr2.address,
      ]);
    });
    it("should not create voting from not owners address", async () => {
      await expect(
        votingSystem
          .connect(addr4)
          .createVoting([addr1.address, addr2.address, addr3.address])
      ).to.be.reverted;
    });
  });

  describe("Voting", () => {
    it("should be possible to vote if pay 0.01 eth", async () => {
      await votingSystem.connect(addr2).vote(0, addr1.address, {
        value: ethers.utils.parseEther("0.01"),
      });
    });
    it("should be impossible to vote if pay less then 0.01 eth", async () => {
      await expect(
        votingSystem.connect(addr1).vote(0, addr1.address, {
          value: ethers.utils.parseEther("0.001"),
        })
      ).to.be.reverted;
    });
    it("should be impossible to vote in finished voting", async () => {
      await expect(
        votingSystem.connect(addr5).vote(1, addr1.address, {
          value: ethers.utils.parseEther("0.01"),
        })
      ).to.be.reverted;
    });
    it("should be impossible to vote if pay more then 0.01 eth", async () => {
      await expect(
        votingSystem.connect(addr1).vote(0, addr2.address, {
          value: ethers.utils.parseEther("0.03"),
        })
      ).to.be.reverted;
    });
    it("should be impossible to vote for free", async () => {
      await expect(votingSystem.connect(addr1).vote(0, addr2.address)).to.be
        .reverted;
    });
    it("should be impossible to vote second time", async () => {
      await expect(
        votingSystem.connect(addr3).vote(0, addr1.address, {
          value: ethers.utils.parseEther("0.01"),
        })
      ).to.be.reverted;
    });
    it("should revert vote if voting is finished", async () => {
      await expect(
        votingSystem.connect(addr6).vote(1, addr1.address, {
          value: ethers.utils.parseEther("0.01"),
        })
      ).to.be.reverted;
    });
    it("should revert vote if there is no candidate with provided address", async () => {
      await expect(
        votingSystem.connect(addr1).vote(0, owner.address, {
          value: ethers.utils.parseEther("0.01"),
        })
      ).to.be.reverted;
    });
  });

  describe("Get voting info", () => {
    it("should return info about existing voting for owner", async () => {
      const voting = await votingSystem.getVotingInfo(0);
      expect(voting).exist;
    });
    it("should return info about existing voting for user", async () => {
      const voting = await votingSystem.connect(addr1).getVotingInfo(0);
      expect(voting).exist;
    });
  });

  describe("Finish voting", () => {
    it("should finish voting by owner after 3 days passed", async () => {
      await ethers.provider.send("evm_increaseTime", [THREE_DAYS]);
      await votingSystem.finishVoting(0);
    });
    it("should not finish voting before 3 days passed", async () => {
      await votingSystem.createVoting([addr1.address, addr2.address]);
      await expect(votingSystem.finishVoting(2)).to.be.reverted;
    });
    it("should not finish finished voting", async () => {
      await expect(votingSystem.finishVoting(1)).to.be.reverted;
    });
    it("should finish voting with not single winner", async () => {
      await votingSystem.createVoting([addr1.address, addr2.address]);
      await ethers.provider.send("evm_increaseTime", [THREE_DAYS]);
      await votingSystem.connect(addr3).vote(2, addr2.address, {
        value: ethers.utils.parseEther("0.01"),
      });
      await votingSystem.connect(addr4).vote(2, addr1.address, {
        value: ethers.utils.parseEther("0.01"),
      });
      await votingSystem.connect(addr5).vote(2, addr1.address, {
        value: ethers.utils.parseEther("0.01"),
      });
      await votingSystem.connect(addr6).vote(2, addr2.address, {
        value: ethers.utils.parseEther("0.01"),
      });
      await votingSystem.finishVoting(2);
    });
  });

  describe("Withdraw comission", () => {
    it("should withdraw profit after voting is finished by owner", async () => {
      await ethers.provider.send("evm_increaseTime", [THREE_DAYS]);
      await votingSystem.finishVoting(0);
      await votingSystem.withdrawComission(owner.address);
    });
    it("should not withdraw profit for user", async () => {
      await ethers.provider.send("evm_increaseTime", [THREE_DAYS]);
      await votingSystem.finishVoting(0);
      await expect(votingSystem.connect(addr1).withdrawComission(addr1.address))
        .to.be.reverted;
    });
  });
});
