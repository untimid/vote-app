require("@nomiclabs/hardhat-waffle");
require("solidity-coverage");
require("dotenv").config();
require("./tasks/VotingSystem");

module.exports = {
  solidity: "0.8.4",
  networks: {
    rinkeby: {
      url: process.env.ALCHEMY_RINKEBY_URL,
      accounts: [process.env.ACCOUNT_PRIVATE_KEY.toString()],
    },
  },
};
