// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.0 <0.9.0;

contract VotingSystem {
    address public owner; // contract deployer
    uint public enteringPrice = .01 ether; // in ETH
    uint private profit = 0 ether; // owners comission
    uint public votingLength = 3 days; // minimum voting length to be able to be finished 

    struct Candidate {
        bool exists;
        address addr;
        uint votesAmount; // how many votes got
        uint infoIndex;
    }

    struct Voting {
        bool active; // active or finished
        uint createdDate; // voting start or created date
        uint endDate; // date after which voting can be finished
        mapping(address => bool) votedUsersMap; // Voters paid for current voting
        address[] candidatesList; // array of candidates
        mapping(address => Candidate) candidatesListMapping; // to access voter from voting list by address
        Candidate[] candidatesInfo; // info for getter 
        address[] winners; // array of winners addresses
        uint winnerVotesCount; // amount of votes to win
    }

    // struct used for return from voting getter
    struct VotingInfo { 
        bool active;
        uint createdDate;
        Candidate[] candidatesInfo;
        address[] winners;
        uint winnerVotesCount;
    }

    Voting[] public votings;

    constructor() {
        owner = msg.sender;
    }

    modifier ownerRightsRequired() {
        require(msg.sender == owner, "You do not have rights to do it!");
        _;
    }

    function createVoting(address[] memory candidateList) public ownerRightsRequired {
        uint index = votings.length;
        votings.push(); 
        Voting storage newVoting = votings[index];
        newVoting.active = true; 
        newVoting.createdDate = block.timestamp;
        newVoting.endDate = block.timestamp + votingLength;
        newVoting.winnerVotesCount = 0;
        for(uint i; i < candidateList.length; i++) {
            address newCandidateAddress = candidateList[i];
            if (!newVoting.candidatesListMapping[newCandidateAddress].exists) {
                Candidate memory newCandidate = Candidate({
                    addr: newCandidateAddress,
                    exists: true,
                    votesAmount: 0,
                    infoIndex: newVoting.candidatesInfo.length
                });
                newVoting.candidatesListMapping[newCandidateAddress] = newCandidate;
                newVoting.candidatesInfo.push(newCandidate);
                newVoting.candidatesList.push(newCandidateAddress);
            }
        }
    }

    function getVotingInfo(uint votingIndex) public view returns(VotingInfo memory)  {
        Voting storage voting = votings[votingIndex];
        VotingInfo memory votingInfo = VotingInfo({
            active: voting.active,
            createdDate: voting.createdDate,
            candidatesInfo: voting.candidatesInfo,
            winners: voting.winners,
            winnerVotesCount: voting.winnerVotesCount
        });
        return votingInfo;
    }

    function vote(uint votingIndex,address candidateAddress) public payable {
        Voting storage targetVoting = votings[votingIndex];
        require(
            msg.value == enteringPrice,
            "You have to pay 0.01 ETH if you want to enter voting."
        );
        require(
            targetVoting.active, 
            "There is no such voting or voting has already been finished!"
            );
        require(
            targetVoting.candidatesListMapping[candidateAddress].exists, 
            "There is no such candidate!"
            );

        require(!targetVoting.votedUsersMap[msg.sender], "You have already voted");

        Candidate storage candidate = targetVoting.candidatesListMapping[candidateAddress];
        targetVoting.votedUsersMap[msg.sender] = true;
        candidate.votesAmount += 1;
        targetVoting.candidatesInfo[candidate.infoIndex] = candidate;
        // handle winners
        if (candidate.votesAmount == targetVoting.winnerVotesCount) {
            targetVoting.winners.push(candidate.addr);
        }
        if (candidate.votesAmount > targetVoting.winnerVotesCount) {
            targetVoting.winnerVotesCount = candidate.votesAmount;
            delete targetVoting.winners;
            targetVoting.winners.push(candidate.addr);
        }
    }
   
    function finishVoting(uint votingIndex) public {
        Voting storage targetVoting = votings[votingIndex];
        require(targetVoting.active, "This voting has already been completed!");
        require(block.timestamp > targetVoting.endDate, "3 days have not already been passed");
        targetVoting.active = false;

        uint budget = enteringPrice * targetVoting.candidatesList.length;
        uint totalPrize = budget * 90 / 100;
        profit += budget - totalPrize;
        uint personalPrize = totalPrize / targetVoting.winners.length;
        for (uint i=0; i < targetVoting.winners.length; i++) {
            payable(targetVoting.winners[i]).transfer(personalPrize);
        }
    }

    function withdrawComission(address to) public ownerRightsRequired {
        payable(to).transfer(profit);
    }
}
