task("createVoting", "Creates voting").setAction(async ({ candidateslist }) => {
  const deployedVotingSystem = await hre.ethers.getContractAt(
    "VotingSystem",
    process.env.RINKEBY_DEPLOYED_CONTRACT_ADDRESS
  );

  try {
    const res = await deployedVotingSystem.createVoting(candidateslist);
    console.log("Voting has been created.", res);
  } catch (err) {
    console.error(err);
  }
});

task("getVotingInfo", "Get info about voting with id")
  .addOptionalParam("votingid", "Which voting to get?")
  .setAction(async ({ votingid }) => {
    const deployedVotingSystem = await hre.ethers.getContractAt(
      "VotingSystem",
      process.env.RINKEBY_DEPLOYED_CONTRACT_ADDRESS
    );

    try {
      const votingInfo = await deployedVotingSystem.getVotingInfo(votingid);
      console.log("Info about voting number ", votingid);
      console.log(votingInfo);
    } catch (err) {
      console.error(err);
    }
  });

task("vote", "Pay 0.01 eth and vote.")
  .addOptionalParam("votingid", "In which voting to vote?")
  .addOptionalParam("candidate", "Your candidate address?")
  .setAction(async ({ votingid, candidate }) => {
    const VotingSystem = await ethers.getContractFactory("VotingSystem");
    const deployedVotingSystem = VotingSystem.attach(
      process.env.RINKEBY_DEPLOYED_CONTRACT_ADDRESS
    );
    try {
      await deployedVotingSystem.vote(votingid, candidate, {
        value: ethers.utils.parseEther("0.01"),
      });
      console.log(
        `Voted successfully in voting ${votingid}, for candidate ${candidate}`
      );
    } catch (err) {
      console.error(err);
    }
  });

task("finishVoting", "Finish voting if 3 days have been passed.")
  .addOptionalParam("votingid", "In which voting to vote?")
  .setAction(async ({ votingid }) => {
    const VotingSystem = await ethers.getContractFactory("VotingSystem");
    const deployedVotingSystem = VotingSystem.attach(
      process.env.RINKEBY_DEPLOYED_CONTRACT_ADDRESS
    );
    try {
      await deployedVotingSystem.finishVoting(votingid);
      console.log(`Voting ${votingid} finished!`);
    } catch (err) {
      console.error(err);
    }
  });

task("withdrawComission", "Take profit!")
  .addOptionalParam("toaddress", "Target address for transfering")
  .setAction(async ({ toaddress }) => {
    const VotingSystem = await ethers.getContractFactory("VotingSystem");
    const deployedVotingSystem = VotingSystem.attach(
      process.env.RINKEBY_DEPLOYED_CONTRACT_ADDRESS
    );
    try {
      await deployedVotingSystem.withdrawComission(toaddress);
      console.log("Comission sent to", toaddress);
    } catch (err) {
      console.error(err);
    }
  });
